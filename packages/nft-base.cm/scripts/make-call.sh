#!/bin/bash
#
set -euo pipefail

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ROOT=$DIR/..

cd $ROOT

node > deploy/.calls-package.yaml <<EOF
const fs = require('fs');

const yamlFilePath = './deploy/calls.yaml.template';
const yamlContent = fs.readFileSync(yamlFilePath, 'utf8');
const modifiedYamlContent = yamlContent.replace(/__CWEB_CONTRACT_SELF_REFERENCE__/g, 'a55bd68971e788f03486a27ceb8ad99497e705e98f0d4e4189dbc6fbe8e59034');

console.log(modifiedYamlContent); // Output the modified YAML content
EOF
