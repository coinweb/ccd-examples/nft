import {
    NewTx,
    getMethodArguments,
    constructContinueTx,
    constructContractIssuer,
    addMethodHandler,
    SELF_REGISTER_HANDLER_NAME,
    executeHandler,
    getContractId,
    constructStore,
    constructClaim,
    constructClaimKey,
    getParameters,
    constructRead,
    constructTake,
    extractRead,
    selfCallWrapper,
    authenticatedWrapper,
    User,
    extractContractInfo,
    extractUser,
    Context,
    ClaimKey,
    constructContractRef,
    getAuthenticated,
    PreparedCallInfo,
    extractContractArgs,
    bigIntReplacer,
    withContinuations,
} from "@coinweb/contract-kit";
import { selfRegisterHandler } from "@coinweb/self-register";
import {
    INIT,
    UPDATE_OWNER,
    UPDATE_OWNER_ACTION,
    UPDATE_OWNER_CONFIRM,
    UPDATE_OWNER_CONFIRM_ACTION,
    VALIDATE_AUTH,
} from "../offchain/constants";

interface ContractConfig {
    owner: User;
}

function ownerKey(): ClaimKey {
    return constructClaimKey("owner", null);
}

function newOwnerKey(): ClaimKey {
    return constructClaimKey("newOwner", null);
}

function validateAuth(context: Context): NewTx[] {
    const [authDataRead] = extractContractArgs(context.tx);
    const authData = extractRead(authDataRead);
    const expected = JSON.stringify(authData[0].content.body, bigIntReplacer);
    const actual = JSON.stringify(getAuthenticated(context.tx).content.body, bigIntReplacer);
    if (expected !== actual) {
        throw new Error(`Invalid auth data, Expected - ${expected}, actual - ${actual}`);
    }
    return [];
}

function prepareValidationCall(context: Context, key: ClaimKey): PreparedCallInfo {
    const self = getContractId(context.tx);
    return {
        ref: constructContractRef(constructContractIssuer(self), []),
        contractInfo: {
            authenticated: extractContractInfo(context.tx).authenticated,
            providedCweb: 1000n,
        },
        methodInfo: {
            methodName: VALIDATE_AUTH,
            methodArgs: []
        },
        contractArgs: [constructRead(constructContractIssuer(self), key)],
    };
}

function prepareUpdateOwnerCall(context: Context, newOwner: User): PreparedCallInfo {
    const self = getContractId(context.tx);
    return {
        ref: constructContractRef(constructContractIssuer(self), []),
        contractInfo: {
            authenticated: null,
            providedCweb: 1000n,
        },
        methodInfo: {
            methodName: UPDATE_OWNER_ACTION,
            methodArgs: [newOwner],
        },
        contractArgs: [],
    }
};

function prepareUpdateOwnerConfirmCall(context: Context): PreparedCallInfo {
    const self = getContractId(context.tx);
    return {
        ref: constructContractRef(constructContractIssuer(self), []),
        contractInfo: {
            authenticated: extractContractInfo(context.tx).authenticated,
            providedCweb: 1000n,
        },
        methodInfo: {
            methodName: UPDATE_OWNER_CONFIRM_ACTION,
            methodArgs: [],
        },
        contractArgs: [],
    }
};

function updateOwner(context: Context): NewTx[] {
    const [_, newOwner] = getMethodArguments(context);
    const validateCall = prepareValidationCall(context, ownerKey());
    const actionCall = prepareUpdateOwnerCall(context, newOwner);
    const callInfo = {
        callInfo: validateCall, 
        continuations: {
            callStack: [{onSuccess: actionCall}]
        }
    };
    return [constructContinueTx(context, [], [callInfo])];
}

function updateOwnerAction(context: Context): NewTx[] {
    const [_, newOwner] = getMethodArguments(context);
    return [
        constructContinueTx(context, [constructStore(constructClaim(newOwnerKey(), newOwner, `0x${0n.toString(16)}`))]),
    ];
}

function updateOwnerConfirm(context: Context): NewTx[] {
    const validateCall = prepareValidationCall(context, newOwnerKey());
    const actionCall = prepareUpdateOwnerConfirmCall(context);
    const callInfo = {
        callInfo: validateCall, 
        continuations: {
            callStack: [{onSuccess: actionCall}]
        }
    };
    return [constructContinueTx(context, [], [callInfo])];
}

function updateOwnerConfirmAction(context: Context): NewTx[] {
    const newOwner = extractUser(getAuthenticated(context.tx));
    return [
        constructContinueTx(context, [
            constructTake(newOwnerKey()),
            constructStore(constructClaim(ownerKey(), newOwner, `0x${0n.toString(16)}`)),
        ]),
    ];
}

function init(context: Context): NewTx[] {
    const parameters: ContractConfig = getParameters("contract/parameters.json");
    const owner = parameters.owner;
    return [
        constructContinueTx(context, [
            constructStore(constructClaim(ownerKey(), JSON.stringify(owner), `0x${0n.toString(16)}`)),
        ]),
    ];
}

export function cwebMain() {
    const module = { handlers: {} };
    addMethodHandler(
        module,
        VALIDATE_AUTH,
        authenticatedWrapper(selfCallWrapper(withContinuations(validateAuth))),
    );
    addMethodHandler(module,UPDATE_OWNER, authenticatedWrapper(updateOwner));
    addMethodHandler(module, UPDATE_OWNER_ACTION, selfCallWrapper(updateOwnerAction));
    addMethodHandler(module, UPDATE_OWNER_CONFIRM, authenticatedWrapper(updateOwnerConfirm));
    addMethodHandler(module, UPDATE_OWNER_CONFIRM_ACTION, selfCallWrapper(updateOwnerConfirmAction));
    addMethodHandler(module, SELF_REGISTER_HANDLER_NAME, selfRegisterHandler);
    addMethodHandler(module, INIT, init);
    executeHandler(module);
}
